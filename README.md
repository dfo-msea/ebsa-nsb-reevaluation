# Re-evaluation of NSB EBSAs

__Main author:__  Jessica Nephin    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564    


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To re-assess the original expert-derived EBSAs within the Northern Shelf Bioregion with available empirical data to increase understanding of the underlying ecological support for the existing EBSAs.


## Summary
Identifying Ecologically and Biologically Significant Areas (EBSAs) is a key component of this commitment and Fisheries and Oceans (DFO) and the Convention on Biological Diversity (CBD) have developed guidelines and criteria to identify these areas. EBSAs were identified in the Northern Shelf Bioregion (NSB) in 2006 using a two-phase expert-driven approach. In response to a science advice request from Oceans Sector, there was a need re-assess the original EBSAs with available empirical data. To assess empirical support for the existing EBSA boundaries, we first summarized species listed as important for each existing EBSA (see Rubidge et al. 2018). Species used to evaluate each EBSA, hereinafter referred to as “important species”, were selected based on the original EBSA reports. Using existing research surveys and commercial data sources, this analysis provided a cursory assessment of empirical support for EBSAs where we had adequate sampling.


## Status
Completed


## Contents
**GroupbyGrid.R**    
Aggregates by grid cell by calculating mean grid cell values for density, diversity and productivity data and calculates the presence of points within grid cell for presence only and polygon layers.

**SpeciesbyEBSA_Overlay.R**   
Assigns species to EBSAs based on IA info and reports. Performs spatial overlay for each species. Adds an inside or outside attribute for each cell for each EBSAs.

**SpeciesbyEBSA_InOutStatistics.R**   
Computes sample and bootstrap statistics by species for each important EBSA and the area outside of all important EBSAs. Uses the percentile bootstrapping method to estimate a 95% confidence interval around the sample statistic.

**SpeciesbyEBSA_Figures.R**   
Produces figures that compare statistics inside and outside EBSAs.

**ProductivitybyEBSA_Overlay.R**   
Performs spatial overlay for each chlorophyll layer. Adds an inside or outside attribute for each raster cell for each EBSAs.

**DiversitybyEBSA_Overlay.R**   
Performs spatial overlay for each diversity metric. Adds an inside or outside attribute for each cell for each EBSAs.

**DivProdbyEBSA_InOutStatistics.R**   
Computes sample and bootstrap statistics for diversity and productivity metrics by species for each EBSA. Uses the percentile bootstrapping method to estimate a 95% confidence interval around the sample statistic.

**DivProdbyEBSA_Figures.R**   
Produces figures that compare diveristy and productivity statistics between EBSAs and outside of all EBSAs.

**Summarise.R**   
Summarises the percent of presence and no data observations for each species by EBSA

**MapEBSAs.R**   
Exports maps of species density and presence as well as and richness and diveristy data aggregated by a 5km grid

**Results_Summary.R**   
Exports table documenting level of support by species for each EBSA.



## Methods
To create an even spatial resolution, species data and the diversity and productivity metrics were aggregated to a 5 by 5 km grid that covered the NSB. Gridding the data ensured equal weights were given to each equal area spatial unit in subsequent calculations. Density, diversity and productivity values were aggregated by grid cell using the mean. Species density or diversity data were only used to evaluate an EBSA if the gridded data covered at least 20% of the EBSA area.

For presence-only data, a grid cell was classified as presence if at least one presence record occurred within the cell. The presence analysis was based on comparing the spatial coverage of species within and outside EBSA boundaries. If an EBSA was deemed important for a particular species, our expectation is that the % presence (similar to % cover) is higher inside the EBSA than outside.

The gridded data were summarized by computing the mean density or % presence by species for the area within an EBSA and the area outside of it. The outside areas were defined as the total NSB area excluding any EBSAs where the species was identified as important for that particular EBSA. For diversity and productivity metrics, which have not been assessed previously, summary statistics were calculated for each EBSA and the area outside of all NSB EBSAs.

Confidence intervals were calculated using a bootstrapping approach. We created a bootstrap distribution (10,000 bootstraps/sample) by calculating the summary statistic (e.g., mean or % presence) of each bootstrap sample drawn from the original “inside EBSA” and “outside EBSA” samples. The 95% confidence intervals were estimated as the interval from the 2.5% to 97.5% of the bootstrap distribution following the percentile method for estimating confidence intervals.


## Requirements
Before running the R scripts that preform the analysis, the following steps must be completed:   
1.  Obtain relevant biological datasets   
2.  Project into BC albers   
3.  Clip data to NSB boundary   
4.  Organise data into geodatabases: Fish.gdb, Invert.gdb, MarineMammals.gdb, MarineBirds.gdb and PolygonRanges.gdb   
5.  Each feature in the geodatabase is a distinct layer (e.g. species, cholorophyll, diversity)   
6.  For survey data features, rename the field of interest with the name of the feature   
7.  Create 5km grid from NSB polygon (with 1km buffer to ensure no data is lost at the coastal boundary)


## Caveats
To explicitly test EBSA boundaries and justify refinement, one must have adequate sampling inside and outside those boundaries. Ideally, one would develop and implement a survey designed to specifically test the validity of the boundary prior to refining it.


## Uncertainty
A major source of uncertainty is the variability in the sampling effort and coverage of the input species data. Maps showing gaps in sampling (no data areas) were clearly shown in the species maps produced. In addition, the sensitivity analysis (\*\_Sensitivity.R) demonstrated that the inside/outside comparison was not very sensitive to unequal sample sizes. In general, as outside sample sizes decreased confidence intervals tended to increase making it more difficult to detect a significant difference between inside and outside EBSAs.


## Acknowledgements
Katie Gale, Emily Rubidge


## References
Rubidge, Emily, Nephin, J, Gale, K.S.P., & Curtis, J. 2018. Reassessment of the Ecologically and Biologically Significant Areas (EBSAs) in the Pacific Northern Shelf Bioregion. DFO Can. Sci. Advis. Sec. Res. Doc. 2018/053. xii + 97 p.
